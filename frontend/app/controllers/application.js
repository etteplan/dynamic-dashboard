import Ember from 'ember';
import Materialize from 'materialize';

export default Ember.Controller.extend({
	// Session injection is required by ember-simple-auth
	session: Ember.inject.service('session'),
	actions: {
		logout() {
			// End the current session
			this.get('session').invalidate()
				.then(function() {
					// Inform the user of successfull logout
					Materialize.toast('Logged out successfully!', 4000, 'light-blue');
				},
				function(error) {
					// Inform the user that something went wrong while logging out
					Materialize.toast('Something went wrong while logging out!', 4000, 'light-red');
					console.log('Error while logging out: ' + error);
				});
			
		}
	}
});