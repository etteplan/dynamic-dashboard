import Ember from 'ember';
import Materialize from 'materialize';

export default Ember.Controller.extend({
	// Clears registration form
	clearForm: function() {
		this.set('email', '');
		this.set('username', '');
		this.set('password', '');
	},

	// Transition user back to login page
	transitionToLogin: function() { this.transitionToRoute('login'); },

	setErrorMessage: function(error) { this.set('errorMessage', error); },

	actions: {
		register: function() {
			// Assign this to self so we are able to address the correct scope
			var self = this;

			// Get user's info from the register form
			let { email, username, password } = self.getProperties('email', 'username', 'password');

			// Create a new user to the database
			var user = self.store.createRecord('user', {
				email: email,
				username: username,
				password: password
			});
			
			// Save user to MongoDB
			user.save()
					// Resolve promise
					.then(function(savedUser) {
					Materialize.toast('Welcome ' + savedUser.get('username') + '! Please login', 4000, 'light-blue');
					console.log(savedUser.get('username') + ' registered successfully');
					self.clearForm();

					// On success, transfer user to login page
					self.transitionToLogin();
				},
				// Reject promise
				function(error) {
					// Show error message
					self.setErrorMessage('E-mail address or username already in use!');
					console.log(error);
					self.clearForm();
				});
		}
	}
});