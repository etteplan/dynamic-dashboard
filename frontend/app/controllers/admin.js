import Ember from 'ember';
import Materialize from 'materialize';

export default Ember.Controller.extend({
	widgetComponent: "empty-form",
	addButtonDisabled: "disabled",
	clearForm: function() {
		this.set('urlParam', '');
		this.set('limit', '');
		this.set('interval', '');
    this.set('authUser', '');
		this.set('title', '');
		this.set('type', '');
		this.set('url', '');
	},

	setErrorMessage: function(error) { this.set('errorMessage', error); },

	actions: {
		changeWidget: function(widget) {
			this.set('widgetComponent', widget);
			this.set('addButtonDisabled', "");
		},
		addWidget: function() {
			console.log("Controller!");
			var self = this;
			// Get widget info from the new widget form
			let { urlParam, limit, interval, authUser, title, type, url } = self.getProperties('urlParam', 'limit', 'interval', 'authUser', 'title', 'type', 'url');

			// Check if any property is undefined and give it an empty string or 0 as value
			if(urlParam === undefined || urlParam === '') {
				urlParam = ' ';
			}
			if(authUser === undefined || authUser === '') {
				authUser = ' ';
			}
			if(limit === undefined || limit === '') {
				limit = 0;
			}
			if(interval === undefined || interval === '') {
				interval = 0;
			}
			// Create a new widget to the database
			var widget = self.store.createRecord('widget', {
				urlParam: urlParam,
        limit: limit,
        interval: interval,
        authUser: authUser,
        widgetTitle: title,
        widgetType: type,
				url: url
			});
			// Save widget
			widget.save().then(function() {
				Materialize.toast('Widget ' + widget.get('widgetTitle') + ' saved to MongoDB.', 4000, 'light-blue');
				self.clearForm();
			},
			function(error) {
				// Show error message
				self.setErrorMessage(error);
			});
		},
		deleteWidget: function(widgetToDelete) {
			if (confirm("Are you sure you want to delete the widget?")) {
				var self = this;
				// Find the widget to delte by id
				self.store.findRecord('widget', widgetToDelete.get('id'), { backgroundReload: false }).then(function(widget) {
					widget.deleteRecord();
					// Check that the widget is actually deleted
					if(widget.get('isDeleted')) {
						Materialize.toast('Widget ' + widget.get('widgetTitle') + ' was deleted successfully!', 4000, 'light-blue');
						widget.save();
						self.clearForm();
					}
					else {
						Materialize.toast('Something went wrong. Widget ' + widget.get('widgetTitle') + ' wasn\'t deleted.', 4000, 'light-blue');
						self.clearForm();
					}
				},
				function(error) {
					// Show error message
					self.setErrorMessage(error);
				});
			}
		},
		editWidget: function(widget) {
			// This function only sets needed properties to edit modal
			var self = this;
			self.set('urlParam', widget.get('urlParam'));
			self.set('limit', widget.get('limit'));
			self.set('interval', widget.get('interval'));
			self.set('authUser', widget.get('authUser'));
			self.set('title', widget.get('widgetTitle'));
			self.set('type', widget.get('widgetType'));
			self.set('id', widget.get('id'));
			self.set('url', widget.get('url'));
		},
		saveWidget: function() {
			var self = this;
			// Get widget properties from the edit modal
			let { urlParam, limit, interval, authUser, title, type, id, url } = self.getProperties('urlParam', 'limit', 'interval', 'authUser', 'title', 'type', 'id', 'url');
			console.log(id);
			// Find the widget to edit and save changes to database
			self.store.findRecord('widget', id).then(function(widget) {
				widget.set('urlParam', urlParam);
        widget.set('limit', limit);
        widget.set('interval', interval);
        widget.set('authUser', authUser);
        widget.set('widgetTitle', title);
        widget.set('widgetType', type);
				widget.set('url', url);
				Materialize.toast('Widget ' + widget.get('widgetTitle') + ' was edited successfully!', 4000, 'light-blue');
				widget.save();
				self.clearForm();
			},
			function(error) {
				// Show error message
				self.setErrorMessage(error);
			});
		}
	}
});
