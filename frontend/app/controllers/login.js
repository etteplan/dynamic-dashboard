import Ember from 'ember';
import Materialize from 'materialize';

export default Ember.Controller.extend({
	// Session injection is required by ember-simple-auth
	session: Ember.inject.service('session'),

	// Displays the error message on the template
	setErrorMessage: function(error) { this.set('errorMessage', error); },

	transitionToAdminPanel: function() { this.transitionToRoute('admin'); },

	actions: {
		authenticate() {
			var self = this;

			// Get the username and password from the login form
			let { username, password } = self.getProperties('username', 'password');

			// Authenticate using ember-simple-auth
			self.get('session').authenticate('authenticator:oauth2', username, password)
				// Resolve promise
				.then(function() {
					// Inform the user of successfull login
					Materialize.toast('Logged in!', 4000, 'light-blue');

					// On success, transfer user to admin panel
					self.transitionToAdminPanel();
				},
				// Reject promise
				function(error) {
					// Display error message below the login form
					self.setErrorMessage('Wrong username or password!');
					console.log('Invalid credentials: ' + error);
				});
		}
	}
});