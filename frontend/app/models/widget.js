import DS from 'ember-data';

export default DS.Model.extend({
  url: DS.attr(),
  urlParam: DS.attr(),
  limit: DS.attr(),
  interval: DS.attr(),
  authUser: DS.attr(),
  widgetTitle: DS.attr(),
  widgetType: DS.attr()
});
