var request     = require('request');
var apiUrl   = 'http://localhost:3000/api';

describe("API", function() {
	describe('GET /api', function() {
		it('Should return status code 200', function(done) {
			request.get(apiUrl, function(error, response, body) {
				expect(response.statusCode).toBe(200);
				done();
			});
		});

		it('Should return API is up & running!', function(done) {
			request.get(apiUrl, function(error, response, body) {
				jasmine.log(body);
				expect(body).toBe('API is up & running!');
				done();
			});
		});
	});
});