// modules
var express		 = require('express');
var mongoose   = require('mongoose');
var passport   = require('passport');
var bodyParser = require('body-parser');
var config 		 = require('./lib/config');

var app				= express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse json
app.use(bodyParser.json());

// set headers
app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, authorization');
	res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
	next();
});

//connect to MongoDB
mongoose.connect(config.get('mongoose:uri'));

//log possible connection error
mongoose.connection.on('error', function (err) {
  console.log('Connection error: ', err.message);
});

//log successful connection
mongoose.connection.once('connected', function() {
	console.log('Connected to database');
});

// configure routes
require('./app/routes')(app);

// start app at http://localhost:3000
app.listen(config.get('port'));
console.log('Running on http://localhost:' + config.get('port'));
