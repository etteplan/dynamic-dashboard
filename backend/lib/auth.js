var config										= require('./config');
var passport									= require('passport');
var BasicStrategy							= require('passport-http').BasicStrategy;
var LocalStrategy 						= require('passport-local').Strategy;
var BearerStrategy						= require('passport-http-bearer').Strategy;
var UserModel									= require('../app/models/user').UserModel;
var AccessTokenModel					= require('../app/models/accessToken').AccessTokenModel;
var RefreshTokenModel					= require('../app/models/refreshToken').RefreshTokenModel;

// Strategy for checking username and password
passport.use(new LocalStrategy(
		function(username, password, done) {
			//Check if user exists in the database
			UserModel.findOne({ username: username }, function(err, user) {
				console.log("Authenticating user: " + username);
				
				//Return possible error message. Usually status code 500
				if (err) { return done(err); }
				
				//User is not found on database. Status code 404
				if (!user) { return done(null, false, { message: 'User not found' }); }

				//User is found but the password is invalid. Status code 404
				if (!user.checkPassword(password)) { return done(null, false, { message: 'Invalid password' }); }
				
				//Credentials are valid, return user
				return done(null, user);
			});
		}
	));

// Strategy for checking access tokens
passport.use(new BearerStrategy(
		function(accessToken, done) {
			//Check if access token exists in the database
			AccessTokenModel.findOne({ token: accessToken }, function(err, token) {

					//Return possible error message. Usually status code 500
					if (err) { return done(err); }

					//Token is not found on database. Status code 404
					if (!token) { return done(null, false); }

					//Check for token expiration
					if (Math.round((Date.now() - token.created) / 1000) > config.get('security:tokenLife')) {
						//Remove expired token
						AccessTokenModel.remove({ token: accessToken }, function(err) {
							if (err) { return done(err); }
						});
						return done(null, false, { message: 'Access token expired' });
					}

					//Check that token owner exists in the database
					UserModel.findById(token.userId, function(err, user) {

						//Return possible error message. Usually status code 500
						if (err) { return done(err); }
						
						//User is not found on database. Status code 404
						if (!user) { return done(null, false, { message: 'Unknown user' }); }

						//Everything checks out, return user
						return done(null, user);
					});
			});
		}
	));