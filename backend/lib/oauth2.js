var oauth2orize								= require('oauth2orize');
var config										= require('./config');
var passport									= require('passport');
var crypto										= require('crypto');
var UserModel									= require('../app/models/user').UserModel;
var AccessTokenModel					= require('../app/models/accessToken').AccessTokenModel;
var RefreshTokenModel					= require('../app/models/refreshToken').RefreshTokenModel;

//Create oauth2orize server
var server = oauth2orize.createServer();

//Exchanges credentials for an access token.
server.exchange(oauth2orize.exchange.password(function(client, username, password, scope, done) {
	//Verify that user exists
	UserModel.findOne({ username: username }, function(err, user) {
		if (err) { return done(err); }
		if (!user) { return done(null, false, { message: 'User not found' }); }
		if (!user.checkPassword(password)) { return done(null, false, { message: 'Invalid password' }); }

		console.log("Serving access token for user: " + username);
		
		//Remove possible old tokens
		RefreshTokenModel.remove({ userId: user.userId }, function (err) {
			if (err) { return done(err); }
		});
		AccessTokenModel.remove({ userId: user.userId }, function (err) {
			if (err) { return done(err); }
		});

		//Create new tokens
		var tokenValue = crypto.randomBytes(128).toString('hex');
		var refreshTokenValue = crypto.randomBytes(128).toString('hex');
		var token = new AccessTokenModel({ token: tokenValue, userId: user.userId });
		var refreshToken = new RefreshTokenModel({ token: refreshTokenValue, userId: user.userId });

		//Save newly created tokens
		refreshToken.save(function(err) {
			if (err) { return done(err); }
		});
		
		token.save(function(err, token) {
			if (err) { return done(err); }
			return done(null, tokenValue, refreshTokenValue, { 'expires_in': config.get('security:tokenLife') });
		});
	});
}));


//Exchanges refreshToken for an access token. Currently not in use, but working
server.exchange(oauth2orize.exchange.refreshToken(function(refreshToken, scope, done) {

	//Verify that given refresh token is valid
	RefreshTokenModel.findOne({ token: refreshToken }, function(err, token) {
		if (err) { return done(err); }
		if (!token) { return done(null, false); }

		//Verify that user exists
		UserModel.findById(token.userId, function(err, user) {
			if (err) { return done(err); }
			if (!user) { return done(null, false); }

			//Removes possible old tokens
			RefreshTokenModel.remove({ userId: user.userId }, function(err) {
				if (err) { return done(err); }
			});
			AccessTokenModel.remove({ userId: user.userId }, function(err) {
				if (err) { return done(err); }
			});

			//Create new tokens
			var tokenValue = crypto.randomBytes(128).toString('hex');
			var refreshTokenValue = crypto.randomBytes(128).toString('hex');
			var token = new AccessTokenModel({ token: tokenValue, userId: user.userId });
			var refreshToken = new RefreshTokenModel({ token: refreshTokenValue, userId: user.userId });

			//Save newly created tokens
			refreshToken.save(function(err) {
				if (err) { return done(err); }
			});
			
			token.save(function(err, token) {
			if (err) { return done(err); }
				done(null, tokenValue, refreshTokenValue, { 'expires_in': config.get('security:tokenLife') });
			});
		});
	});
}));

exports.token = [
	server.token(),
	server.errorHandler()
];