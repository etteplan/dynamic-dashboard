var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

//Define widget schema
var Widget = new Schema({
	url: {
		type: String,
		required: true
	},
	urlParam: {
		type: String,
		required: true
	},
	limit: {
		type: Number,
		required: true
	},
	interval: {
		type: Number,
		required: true
	},
	authUser: {
		type: String,
		required: true
	},
	widgetTitle: {
		type: String,
		required: true
	},
	widgetType: {
		type: String,
		required: true
	}
});

//Return widget id
Widget.virtual('widgetId').get(function() {
	return this.id;
});

//Create model for widget
var WidgetModel = mongoose.model('Widget', Widget);

module.exports.WidgetModel = WidgetModel;
