var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Define refresh token schema
var RefreshToken = new Schema({
	userId: {
		type: String,
		required: true
	},
	token: {
		type: String,
		unique: true,
		required: true
	},
	created: {
		type: Date,
		default: Date.now
	}
});

//Create model for refresh token
var RefreshTokenModel = mongoose.model('RefreshToken', RefreshToken);

module.exports.RefreshTokenModel = RefreshTokenModel;