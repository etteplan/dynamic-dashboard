var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Define access token schema
var AccessToken = new Schema({
	userId: {
		type: String,
		required: true
	},
	token: {
		type: String,
		unique: true,
		required: true
	},
	created: {
		type: Date,
		default: Date.now
	}
});

//Create model for access token
var AccessTokenModel = mongoose.model('AccessToken', AccessToken);

module.exports.AccessTokenModel = AccessTokenModel;