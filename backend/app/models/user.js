var crypto = require('crypto');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Define user schema
var User = new Schema({
	username: {
		type: String,
		unique: true,
		required: true
	},
	email: {
		type: String,
		unique: true,
		required: true
	},
	hashedPassword: {
		type: String,
		required: true
	},
	salt: {
		type: String,
		required: true
	},
	verified: {
		type: Boolean,
		required: true
	}
});

//Encrypt password
User.methods.encryptPassword = function(password) {
	return crypto.createHmac('sha256', this.salt).update(password).digest('hex');
}

//Return user id
User.virtual('userId').get(function() {
	return this.id;
});

//Start password encryption flow
User.virtual('password').set(function(password) {
	this.plainPassword = password;
	this.salt = crypto.randomBytes(128).toString('hex');
	this.hashedPassword = this.encryptPassword(password);
}).get(function() {
	return this.plainPassword;
});

//Check password
User.methods.checkPassword = function(password) {
	return this.encryptPassword(password) == this.hashedPassword;
}

//Create model for user
var UserModel = mongoose.model('User', User);

module.exports.UserModel = UserModel;