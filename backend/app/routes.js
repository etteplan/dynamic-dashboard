require('../lib/auth');
var oauth2 		  = require('../lib/oauth2');
var passport	  = require('passport');
var WidgetModel = require('./models/widget').WidgetModel;
var UserModel	  = require('./models/user').UserModel;

module.exports = function(app) {

	// init passport
	app.use(passport.initialize());

	// request access token
	app.post('/oauth/token', oauth2.token);

	// register new user
	app.post('/api/users', function(req, res) {
		var user = new UserModel({
			username: req.body.user.username,
			password: req.body.user.password,
			email: req.body.user.email,
			verified: true
		});

		user.save(function(err, user) {
			if (!err) {
				console.log('New user created: ' + user);
				res.send({ user: user });
			}
			else {
				console.log('Error while saving user to database: ' + err);
				res.status(500).send({ error: err});
			}
		});
	});

	// Used in api_spec.js test for checking server status
	app.get('/api', function(req, res) {
		res.send('API is up & running!');
	});

	// Get all widgets
	app.get('/api/widgets',
	passport.authenticate('bearer', { session: false }),
	function(req, res) {
		return WidgetModel.find(function (err, widget) {
			if (!err) {
				return res.send({ widget: widget} );
			} else {
				return res.status(500).send({ error: err });
			}
		});
	});

	// Create a new widget
	app.post('/api/widgets',
	passport.authenticate('bearer', { session: false }),
	function(req, res) {
		var widget = new WidgetModel({
			url: req.body.widget.url,
			urlParam: req.body.widget.urlParam,
			limit: req.body.widget.limit,
			interval: req.body.widget.interval,
			authUser: req.body.widget.authUser,
			widgetTitle: req.body.widget.widgetTitle,
			widgetType: req.body.widget.widgetType
		});
		widget.save(function (err, widget) {
			if (!err) {
				console.log('New widget created: ' + widget);
				return res.send({ widget: widget });
			} else {
				console.log('Error while saving widget to database: ' + err);
				return res.status(500).send({ error: err });
			}
		});
	});

	// Get widget by ID
	app.get('/api/widgets/:id',
	passport.authenticate('bearer', { session: false }),
	function(req, res) {
		return WidgetModel.findById(req.params.id, function(err, widget) {
			if (!widget) {
				return res.status(404).send({ error: 'Widget not found!' });
			}
			if (!err) {
				return res.send({ widget: widget });
			} else {
				return res.status(500).send({ error: err });
			}
		});
	});

	// Update widget
	app.put('/api/widgets/:id',
	passport.authenticate('bearer', { session: false }),
	function(req, res) {
		return WidgetModel.findById(req.params.id, function(err, widget) {
			if (!widget) {
				return res.status(404).send({ error: 'Widget not found!' });
			}
			widget.url = req.body.widget.url;
			widget.urlParam = req.body.widget.urlParam;
			widget.limit = req.body.widget.limit;
			widget.interval = req.body.widget.interval;
			widget.authUser = req.body.widget.authUser;
			widget.widgetTitle = req.body.widget.widgetTitle;
			widget.widgetType = req.body.widget.widgetType;

			return widget.save(function (err) {
				if (!err) {
					return res.send({ widget: widget });
				} else {
					return res.status(500).send({ error: err });
				}
			});
		});
	});

	// Delete widget
	app.delete('/api/widgets/:id',
	passport.authenticate('bearer', { session: false }),
	function(req, res) {
		return WidgetModel.findById(req.params.id, function(err, widget) {
			if (!widget) {
				return res.status(404).send({ error: 'Widget not found!' });
			}
			return widget.remove(function (err) {
				if (!err) {
					return res.send({ status: 'Widget removed!' });
				} else {
					return res.status(500).send({ error: err });
				}
			})
		});
	});
};
