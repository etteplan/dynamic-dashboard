widget = {
  //runs when we receive data from the job
  onData: function(el, data) {
    $('.content', el).html("");
    if (data.json) {
      var index = 0;
      data.json.distinct.forEach(function (issue) {
        $('.content', el).append(
          "<div class='item-container'>" +
            "<div class='issue'>" + issue + "</div> " +
            "<div class='frequency'>" + data.json.count[index] + "</div>" +
          "</div>"
        );
        index++;
      });
    }
  }
};
