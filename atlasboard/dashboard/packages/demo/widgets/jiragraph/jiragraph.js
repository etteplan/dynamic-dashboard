widget = {
  //runs when we receive data from the job
  onData: function(el, data) {
    $('.content', el).html("");
    if (data.json) {
      generateChart(data.json);
    }

    function generateChart(obj) {
    	var ctx = document.getElementById("myCanvas").getContext("2d");

    	var data = {
    		labels: obj.distinct,
    		datasets: [
    			{
    				label: "Issue statuses",
    				backgroundColor: "rgba(75,192,192,0.4)",
    				fillColor: "rgba(220,220,220,0.2)",
    				strokeColor: "rgba(75, 192, 192, 0.2)",
    				pointColor: "rgba(220,220,220,1)",
    				pointStrokeColor: "#fff",
    				pointHighlightFill: "#fff",
    				pointHighlightStroke: "rgba(220,220,220,1)",
    				data: obj.count
    			},
    		]
    	};

    	// Instantiate a new chart using 'data' (defined below)
    	var myChart = new Chart(ctx, {
    		type: 'bar',
    		data: data,
    		options: {
    			responsive: true,
    			scales: {
    				yAxes: [{
    					ticks: {
    						beginAtZero: true,
                fontSize: 40,
                fontColor: "#FFF"
    					}
    				}],
            xAxes: [{
    					ticks: {
                fontSize: 40,
                fontColor: "#FFF"
    					}
    				}]
    			},
          legend: {
            display: false
          }
    		}
    	});
    }
  }
};
