module.exports = function (config, dependencies, job_callback) {

   var FeedParser = require('feedparser');
    var request = require('request');
    var logger = dependencies.logger;
    var newsparts = {};
    var news = [];

  var req = request('http://www.iltalehti.fi/rss/uutiset.xml')
    var feedparser = new FeedParser();

   req.on('error',function(error){

   });

 req.on('response', function (res) {
    var stream = this; // `this` is `req`, which is a stream

   if (res.statusCode !== 200) {
      this.emit('error', new Error('Bad status code'));
    }
    else {
      stream.pipe(feedparser);
    }
  });

feedparser.on('error', function (error) {
  // always handle errors
});

feedparser.on('readable', function () {
  // This is where the action is!
  var stream = this; // `this` is `feedparser`, which is a stream
  var meta = this.meta; // **NOTE** the "meta" is always available in the context of the feedparser instance
  var item;
  var ep;

  while ((item = stream.read()) !== null) {
    ep = {
          'title' : item.title,
          'desc' : item.description,
          'link' : item.link
    };

   news.push(ep);

 }

});

   dependencies.easyRequest.HTML('http://google.com', function (err, html) {
      // logger.trace(html);
      console.log(news[0].title);
      job_callback(err, {title: config.widgetTitle, html: html, news: news });
    });

};
