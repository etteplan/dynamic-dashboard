/**
 * Job: jirajob
 *
 * Expected configuration:
 *
 * ## PLEASE ADD AN EXAMPLE CONFIGURATION FOR YOUR JOB HERE
 * {
 *   myconfigKey : [
 *     { serverUrl : 'localhost' }
 *   ]
 * }
 */

var qs = require('querystring');

module.exports = function (config, dependencies, job_callback) {
  var logger = dependencies.logger;
  var options = {};
  var baseUrl;
  var clickUrl;

  for (link in config.links) {
    if (!config.links[link].url) {
      return job_callback("No JIRA server configured");
    }
    if (!config.urlParam) {
      return job_callback("No JQL configured");
    }
    if (config.links[link].title == "JIRA") {
      baseUrl = config.links[link].url + '/rest/api/2/search?';
      clickUrl = config.links[link].url + "/issues/?";
    }
  }

  if (config.authUser && (config.globalAuth && config.globalAuth[config.authUser])) {
    options.headers = {
        "authorization": "Basic " + new Buffer(config.globalAuth[config.authUser].username
          + ":" + config.globalAuth[config.authUser].password).toString("base64")
      }
  }

  options.url = baseUrl + qs.stringify({jql: config.urlParam, expand: "changelog", maxResults: config.limit});
  logger.log("Headers are: " + options.headers);

  dependencies.easyRequest.JSON(options, function(err, data) {
    if (err) {
      return job_callback(err);
    }
    if (data.issues == null || data.issues == undefined) {
      return job_callback('jira_server returned a malformed JSON response');
    }
    var jsonData = parseJSON(data);
    job_callback(null, {json: jsonData});
  });

  function parseJSON(data) {
    var statuses = {};
    var obj = data;
    for (issue in obj["issues"]) {
      var issueData = obj["issues"][issue];
      for (log in issueData["changelog"]["histories"]) {
        var logData = issueData["changelog"]["histories"][log];
        for (item in logData["items"]) {
          var itemData = logData["items"][item];
          if (itemData["field"].toUpperCase() == "STATUS") {
            statuses[issueData["key"]] = itemData["toString"];
          }
        }
      }
    }
    var distinctStatuses = [];
    var uniqueStatuses = {};
    var arr = Object.keys(statuses).map(function (key) { return statuses[key];});
    arr.forEach(function (x) {
      if (!uniqueStatuses[x]) {
        distinctStatuses.push(x);
        uniqueStatuses[x] = true;
      }
    });
    distinctStatuses.sort();
    var counts = countOccurrences(arr);
    var statusCounts = Object.keys(counts).map(function (key) { return counts[key];});

    var distinctIssues = {
      distinct: distinctStatuses,
      count: statusCounts
    };
    return distinctIssues;

  }

  function countOccurrences(arr) {
    var counts = {};
    arr.sort();
    for(var i = 0; i< arr.length; i++) {
      var value = arr[i];
      counts[value] = counts[value] ? counts[value]+1 : 1;
    }
    return counts;
  }
};
